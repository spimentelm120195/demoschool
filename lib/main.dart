import 'package:final_edu/add_student_page.dart';
import 'package:final_edu/menu_page.dart';
import 'package:final_edu/stories_page.dart';
import 'package:final_edu/student_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TabController = new DefaultTabController(
      length: 4,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text('App de Prueba'),
          bottom: new TabBar(
            indicatorColor: Colors.blue,
            tabs: <Widget>[
              new Tab(icon: new Icon(Icons.payment),text: 'Avisos'),
              new Tab(icon: new Icon(Icons.message),text: 'Chat'),
              new Tab(icon: new Icon(Icons.notifications),text: 'Notificaciones'),
              new Tab(icon: new Icon(Icons.menu),text: 'Menu',)
          ]),
        ),
        body: new TabBarView(
          children: <Widget>[
            new StudentPage(),
            new AddStudentPage(),
            new StoriesPage(),
            new MenuPage()
          ]
        )
      ),
      );
    return new MaterialApp(
      title: 'Primera Prueba',
      theme: new ThemeData.light(),
      home: TabController,
    );
  }
}
import 'package:final_edu/add_student_page.dart';
import 'package:flutter/material.dart';

class StudentPage extends StatelessWidget {
  List<String> _clases= ['Clase 1','Clase 2','Clase 3'];
  String _claseSeleccionada = 'Clase 1';
  List<String> _secciones = ['Seccion A','Seccion B','Seccion C'];
  String _seccionSeleccionada = 'Seccion A';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Estudiante'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 20.0),
        children: <Widget>[
          titulo('Seleccione una Clase: '),
          Divider(),
          _crearDropdown(_clases,_claseSeleccionada),
          Divider(),
          titulo('Seleccione una Seccion: '),
          Divider(),
          _crearDropdown(_secciones,_seccionSeleccionada),
          Divider(),
          const RaisedButton(
            color: Colors.blue,
            onPressed: null,
            shape: StadiumBorder(),
            child: Text(
              'Filtrar',
              style: TextStyle(fontSize: 20,color: Colors.white)
            ),
          ),
          _listaRegistros(),
          
        ]
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_circle),
          onPressed: (){
              Navigator.push(context,MaterialPageRoute(builder: (context) => AddStudentPage()),
              );
            },
        ),
    );
  }
  Widget titulo(String text){
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(text, style: TextStyle(color: Colors.black,fontSize: 25.0),),
            SizedBox(height: 10.0),
            //Text('Selecciona una de las opciones a la cual deseas ingresar',style: TextStyle(color: Colors.black,fontSize: 16.0),)
          ]
        )
      ),
    );
  }
  List<DropdownMenuItem<String>> getOpcionesDropdown(List<String> elementos){
    List<DropdownMenuItem<String>> lista = new List();
    elementos.forEach((elemento){
      lista.add(DropdownMenuItem(
        child: Text(elemento,textAlign: TextAlign.center),
        value: elemento,
      ));
    });
    return lista;
  }
  Widget _crearDropdown(List<String> d, String selec){
    return DropdownButton(
      value: selec,
      //isExpanded: true, 
      items: getOpcionesDropdown(d),
      onChanged: (opt){
        //print(opt);
        //_claseSeleccionada = opt;
      },
    );
  }
  Widget _listaRegistros(){
    /*return ListTile(
            title: Text('Registros'),
          );*/
      return Card(
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Miguel A.'),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text('015412-124512'),
            ),
            RaisedButton(
              color: Colors.blue,
              onPressed: null,
              shape: StadiumBorder(),
              child: Text(
                'Filtrar',
              style: TextStyle(fontSize: 20,color: Colors.white)
            )
            )
          ]
        )
      );
  }
}

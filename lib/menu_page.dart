import 'package:final_edu/add_student_page.dart';
import 'package:final_edu/mapok_page.dart';
import 'package:flutter/material.dart';

import 'map_page.dart';
class MenuPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _titulo(),
                _botonRedondo(context),
              ],
            )
          )
        ]
      ),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }
  Widget _fondoApp(){

    final gradiente  = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        color: Colors.black45,
        gradient: LinearGradient(
          begin: FractionalOffset(1.0, 0.9),
          end: FractionalOffset(0.0, 0.25),
          colors: [
            Colors.white,
            Colors.blue
          ],
        )
      ),
    );
    final second  = Container(
      width: 360.0,
      height: 360.0,
      decoration: BoxDecoration(
        color: Colors.blueGrey
      ),
    );
    return Stack(
      children: <Widget>[
        gradiente
        //second
      ]
    );
  }
  Widget _titulo(){
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          ]
        )
      ),
    );
  }
  Widget _bottomNavigationBar(BuildContext context){
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Colors.blue,
        primaryColor: Colors.white
      ),
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard, size: 28.0),
            title: Container()
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings, size: 28.0),
            title: Container()
          )
        ],
      ),
    );
  }
  Widget _botonRedondo(BuildContext context){
    return Table(
      children: [
        TableRow(
          children: [
            _crearBoton(context,Colors.white,Icons.account_circle,'Usuarios',AddStudentPage()),
            _crearBoton(context,Colors.white,Icons.account_balance,'Academico',AddStudentPage())
          ]
        ),
        TableRow(
          children: [
            _crearBoton(context,Colors.white,Icons.event_note,'Examenes',AddStudentPage()),
            //_crearBoton(context,Colors.white,Icons.camera_alt,'Camara',CameraPage())
            Container(
              height: 150.0,
              margin: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(25.0)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  CircleAvatar(
                    radius: 25.0,
                    child: IconButton(
                        icon: Icon(Icons.camera_alt,color: Colors.white,),
                        onPressed: (){
                          Navigator.push(context,MaterialPageRoute(builder: (context) => AddStudentPage()));
                    }),
                  ),
                  Text('Camara', style: TextStyle(color: Colors.white)),
                  SizedBox(height: 5.0),
                ],
              ),
            )
          ]
        ),
        TableRow(
          children: [
            _crearBoton(context,Colors.white,Icons.book,'Libreria',MapPage()),
            _crearBoton(context,Colors.white,Icons.message,'Mensajería',MapokPage())
          ]
        )
      ]
    );
  }

  Widget _crearBoton(BuildContext context,Color color,IconData icono, String texto,Widget route){
    return Container(
      height: 150.0,
      margin: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.circular(25.0)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          CircleAvatar(
            radius: 25.0,
            child: IconButton(
                icon: Icon(icono,color: color,),
                onPressed: (){
                  Navigator.push(context,MaterialPageRoute(builder: (context) => route));
            }),
          ),
          Text(texto, style: TextStyle(color: color)),
          SizedBox(height: 5.0),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';

class StoriesPage extends StatefulWidget {
  StoriesPage({Key key}) : super(key: key);

  @override
  _StoriesPageState createState() => _StoriesPageState();
}

class _StoriesPageState extends State<StoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 180,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[

                        ],
                      ),
                    ),
                    SizedBox(height: 15,),
                    makeFeed(
                      userName: 'Jose Px',
                      userImage: 'assets/Jose.jpg',
                      feedTime: 'Hace 1 hora',
                      feedText: 'Comunicado a los Padres:'+
                                'La siguiente semana son los examenes para todos los grados y secciones '+
                                'por favor tener en cuenta esto.',
                      feedImage: 'assets/examen.jpg'
                    ),
                    makeFeed(
                      userName: 'Miguel Dz',
                      userImage: 'assets/Miguel.jpg',
                      feedTime: 'Hace 2 horas',
                      feedText: 'Aviso:'+
                                'Se ha encontrado algunas pertenencias en el salon ... '+
                                'por favor si le pertenece a alguno, llamar.',
                      feedImage: 'assets/cuaderno.jpg'
                    ),
                    makeFeed(
                      userName: 'Criss Xo',
                      userImage: 'assets/Criss.jpg',
                      feedTime: 'Hace 3 horas',
                      feedText: 'Recordatorio:'+
                                'Recordar que la sigte semana .... . . . .. .  '+
                                'por favor tener en cuenta esto.',
                      feedImage: 'assets/recordatorio.jpg'
                    ),
                    makeFeed(
                      userName: 'Luis Ch',
                      userImage: 'assets/Luis.jpg',
                      feedTime: 'Hace media hora',
                      feedText: 'Se les hace presente ..... ... :'+
                                'El alumno ...... debe ......... '+
                                'por favor contactarme al numero 985658475.',
                      feedImage: 'assets/celular.jpg'
                    ),
                  ]
                ),
              )
            ),
          )
        ]
      ),
    );
  }

  Widget makeFeed({userName,userImage,feedTime,feedText,feedImage}) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(userImage),
                        fit: BoxFit.cover
                      )
                    )
                  ),
                  SizedBox(width:15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(userName, style: TextStyle(color: Colors.grey,fontSize: 16),),
                      SizedBox(height: 4),
                      Text(feedTime,style: TextStyle( color:Colors.grey[100],fontSize: 12),),
                    ],
                  )
                ],
              ),
              IconButton(
                icon: Icon(Icons.more_horiz,size: 30,color: Colors.grey[600],),
                onPressed: (){},
              )
            ],
          ),
          SizedBox(height: 20,),
          Text(feedText,style: TextStyle(fontSize:15,color: Colors.grey[800],height: 1.5,letterSpacing: .7 ),),
          SizedBox(height:20,),
          Container(
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(feedImage),
                fit: BoxFit.cover,
              ) 
            )
          ),
          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              Row(
                children: <Widget>[
                  likes(),
                ],)
            ],
          )
        ]
      ),
    );
  }
  Widget likes(){
    return Container(
      width: 25,
      height: 25,
      decoration: BoxDecoration(
        color: Colors.blue,
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white)
      ),
      child: Center(
        child: Icon(Icons.thumb_up,size: 12,color: Colors.white),
      ),
    );
  }
}
import 'package:flutter/material.dart';
class AddStudentPage extends StatelessWidget{
  /*String _nombre='';
  String _email='';
  String _fecha='';
  TextEditingController _inputEditingController = new TextEditingController();
  */
  List<String> _padres= ['Juan Manuel','Luis Bustinza','Miguel Quispe'];
  String _padreSeleccionado = 'Juan Manuel';
  List<String> _clases= ['Clase 1','Clase 2','Clase 3'];
  String _claseSeleccionada = 'Clase 1';
  List<String> _secciones = ['Seccion A','Seccion B','Seccion C'];
  String _seccionSeleccionada = 'Seccion A';
  List<String> _generos= ['Masculino','Femenino'];
  String _generoSeleccionado = 'Masculino';
  List<String> _gsanguineo= ['A+','A-','B+','B-','AB+','AB-','O+','O-'];
  String _gsanguineoSeleccionado = 'O+';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Añadir Alumno'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 20.0),
        children: <Widget>[
          _crearEntradas(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          Text('Padre: '),
          _crearDropdown(_padres, _padreSeleccionado),
          Divider(),
          Text('Clase: '),
          _crearDropdown(_clases, _claseSeleccionada),
          Divider(),
          Text('Sección: '),
          _crearDropdown(_secciones, _seccionSeleccionada),
          Divider(),
          Text('Género: '),
          _crearDropdown(_generos, _generoSeleccionado),
          Divider(),
          Text('Grupo Sanguíneo: '),
          _crearDropdown(_gsanguineo, _gsanguineoSeleccionado),
          Divider(),
          TextField(
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0)
              ),
              labelText: 'Dirección: ',
              hintText: 'Dirección Completa',
              icon: Icon(Icons.home),
              suffixIcon: Icon(Icons.home),
            ),
          ),
          Divider(),
          _crearFono(),
          Divider(),
          RaisedButton(
            onPressed: () {},
            color: Colors.blue,
            textColor: Colors.white,
            child: const Text(
              'Registrar',
              style: TextStyle(fontSize: 20
              )
            ),
          ),
          //_crearFecha(context),
        ],
        )
    );
 }
 Widget _crearEntradas(){
   return TextField(
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0) 
       ),
       hintText: 'Nombres Completos',
       labelText: 'Nombres',
       suffixIcon: Icon(Icons.accessibility_new),
       icon: Icon(Icons.account_circle),
     ),
   );
 }
 Widget _crearFono(){
   return TextField(
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       border: OutlineInputBorder(
         borderRadius: BorderRadius.circular(20.0) 
       ),
       hintText: '9 dígitos',
       labelText: 'N° Celular',
       suffixIcon: Icon(Icons.phone_android),
       icon: Icon(Icons.phone_android),
     ),
   );
 }
 Widget _crearEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        labelText: 'Email',
        suffixIcon: Icon(Icons.alternate_email),
        icon: Icon(Icons.email) 
      ),
    );
  }
  Widget _crearPassword() {
    return TextField(
      obscureText: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        labelText: 'Password',
        suffixIcon: Icon(Icons.lock_open),
        icon: Icon(Icons.lock) 
      ),
    );
  }
  /*Widget _crearFecha(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputEditingController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        labelText: 'Fecha de Nacimiento',
        suffixIcon: Icon(Icons.calendar_today),
        icon: Icon(Icons.email) 
      ),
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }
  /*_selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2020),
      lastDate: new DateTime(2025),
      locale: Locale('es','ES'),
    );
  }*/
}*/
  List<DropdownMenuItem<String>> getOpcionesDropdown(List<String> elementos){
    List<DropdownMenuItem<String>> lista = new List();
    elementos.forEach((elemento){
      lista.add(DropdownMenuItem(
        child: Text(elemento,textAlign: TextAlign.center),
        value: elemento,
      ));
    });
    return lista;
  }
  Widget _crearDropdown(List<String> d, String selec){
    return DropdownButton(
      value: selec,
      //isExpanded: true, 
      items: getOpcionesDropdown(d),
      onChanged: (opt){
        //print(opt);
        //_claseSeleccionada = opt;
      },
    );
  }
}